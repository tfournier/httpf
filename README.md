# HTTPF

Simple HTTP files server with UI for list entries.

## Configuration

| Flag            | Env                   | Type     | Description                  | Default             |
|-----------------|-----------------------|----------|------------------------------|---------------------|
| debug           | HTTPF_DEBUG           | boolean  | Enable debug mode            | false               |
| listen          | HTTPF_LISTEN          | string   | Listen address with port     | :8888               |
| display         | HTTPF_DISPLAY         | string   | Display mode (grid or table) | grid                |
| preview         | HTTPF_PREVIEW         | string   | Enable preview               | true                |
| ignore          | HTTPF_IGNORE          | []string | List of ignored files        | .DS_Store, .noindex |
| skip-extensions | HTTPF_SKIP_EXTENSIONS | []string | List of extensions skipped   | .nfo                |
