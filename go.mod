module gitlab.com/tfournier/httpf

go 1.20

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/urfave/cli/v2 v2.25.6
	gitlab.com/evolves-fr/gommon v0.44.1
)

require (
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
