package main

import (
	_ "embed"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/gommon"
)

const (
	displayGrid  = "grid"
	displayTable = "table"
)

var (
	version = "dev"

	//go:embed template_grid.gohtml
	templateGrid string

	//go:embed template_table.gohtml
	templateTable string

	//go:embed favicon.ico
	favicon []byte

	pdfExt        = []string{".pdf"}
	csvExt        = []string{".csv"}
	textExt       = []string{".txt", ".md", ".log"}
	imageExt      = []string{".jpg", ".jpeg", ".png", ".gif", ".svg", ".tiff", ".webp"}
	audioExt      = []string{".mp3"}
	videoExt      = []string{".mp4", ".avi", ".mkv"}
	archiveExt    = []string{".zip", ".gzip", ".gz", ".7z", ".tar"}
	securityExt   = []string{".asc", ".crt", ".csr", ".key"}
	codeExt       = []string{".sh", ".html", ".mhtml", ".css", ".js", ".yml", ".yaml", ".json", ".xml", ".sql", ".conf"}
	wordExt       = []string{".doc", ".docm", ".docx"}
	excelExt      = []string{".xls", ".xlsb", ".xlsx"}
	powerpointExt = []string{".ppt", ".pptm", ".pptx"}
)

type (
	Entry struct {
		IsDir bool
		Name  string
		Path  string
		Image string
		Icon  string
		Size  string
		Time  time.Time
	}
)

func init() {
	logrus.SetOutput(os.Stdout)
	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.TextFormatter{FullTimestamp: true})
}

func main() {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Enable debug mode",
			Aliases: []string{"d"},
			EnvVars: []string{"HTTPF_DEBUG"},
			Value:   false,
		},
		&cli.StringFlag{
			Name:    "listen",
			Usage:   "Listen address",
			Aliases: []string{"l"},
			EnvVars: []string{"HTTPF_LISTEN"},
			Value:   ":8888",
		},
		&cli.StringFlag{
			Name:    "display",
			Usage:   "Display mode",
			Aliases: []string{},
			EnvVars: []string{"HTTPF_DISPLAY"},
			Value:   "grid",
		},
		&cli.StringFlag{
			Name:    "order",
			Usage:   "Order mode",
			Aliases: []string{},
			EnvVars: []string{"HTTPF_ORDER"},
			Value:   "name",
		},
		&cli.BoolFlag{
			Name:    "preview",
			Usage:   "Enable preview",
			Aliases: []string{},
			EnvVars: []string{"HTTPF_PREVIEW"},
			Value:   true,
		},
		&cli.StringSliceFlag{
			Name:    "ignore",
			Usage:   "List of ignored files",
			Aliases: []string{},
			EnvVars: []string{"HTTPF_IGNORE"},
			Value:   cli.NewStringSlice(".DS_Store", ".noindex"),
		},
		&cli.StringSliceFlag{
			Name:    "skip-extensions",
			Usage:   "List of extensions skipped",
			Aliases: []string{},
			EnvVars: []string{"HTTPF_SKIP_EXTENSIONS"},
			Value:   cli.NewStringSlice(".nfo"),
		},
	}

	app := &cli.App{
		Name:      "http-files",
		Usage:     "Serve local directory",
		Copyright: "EVOLVES SAS",
		Authors:   []*cli.Author{{Name: "Thomas FOURNIER", Email: "tfournier@evolves.fr"}},
		Version:   version,
		Flags:     flags,
		Before:    before,
		Action:    action,
	}

	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}

func before(c *cli.Context) error {
	if c.Bool("debug") {
		logrus.SetLevel(logrus.DebugLevel)
	}

	if !gommon.Has(c.String("display"), displayGrid, displayTable) {
		return errors.New("invalid display mode (only grid or table)")
	}

	return nil
}

func action(c *cli.Context) error {
	return http.ListenAndServe(c.String("listen"), New(Config{
		Root:           c.Args().First(),
		Preview:        c.Bool("preview"),
		Display:        c.String("display"),
		Order:          c.String("order"),
		Ignore:         c.StringSlice("ignore"),
		SkipExtensions: c.StringSlice("skip-extensions"),
	}))
}

type Config struct {
	Root           string
	Preview        bool
	Display        string
	Order          string
	Ignore         []string
	SkipExtensions []string
}

func New(cfg Config) http.Handler {
	return handler{
		Config: cfg,
		templates: map[string]*template.Template{
			displayGrid:  template.Must(template.New("index.html").Parse(templateGrid)),
			displayTable: template.Must(template.New("index.html").Parse(templateTable)),
		},
	}
}

type handler struct {
	Config
	templates map[string]*template.Template
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.RequestURI == "/favicon.ico" {
		w.Write(favicon)
		return
	}

	// Verify root directory exist
	if _, err := os.Stat(h.Root); os.IsNotExist(err) {
		h.error(w, r, http.StatusInternalServerError, err)
		return
	}

	// Decode request uri
	uri, err := url.PathUnescape(r.URL.Path)
	if err != nil {
		h.error(w, r, http.StatusBadRequest, err)
		return
	}

	// Join root directory with request uri decoded
	path := filepath.Join(h.Root, uri)

	// Verify final path exist
	stat, err := os.Stat(path)
	if os.IsNotExist(err) {
		h.error(w, r, http.StatusNotFound, err)
		return
	}

	// If file requested, serve it
	if !stat.IsDir() {
		http.ServeFile(w, r, path)
		return
	}

	// Get query param for order or use fallback
	order := gommon.Default(r.URL.Query().Get("order"), h.Order)

	// Get query param for preview or use fallback
	preview := h.Preview
	if v := r.URL.Query().Get("preview"); !gommon.Empty(v) {
		preview = gommon.MustParse[bool](v)
	}

	// Get directory entries
	entries, err := h.find(path, order, preview)
	if err != nil {
		h.error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.result(w, r, entries)
}

func (h handler) result(w http.ResponseWriter, r *http.Request, entries []Entry) {
	data := map[string]any{
		"entries": entries,
		"queries": r.URL.Query(),
	}

	if err := h.templates[gommon.Default(r.URL.Query().Get("display"), h.Display)].Execute(w, data); err != nil {
		h.error(w, r, http.StatusInternalServerError, err)
		return
	}

	h.error(w, r, http.StatusOK, nil)
}

func (h handler) error(w http.ResponseWriter, r *http.Request, code int, err error) {
	if err != nil {
		http.Error(w, err.Error(), code)
	}

	var lvl logrus.Level
	switch {
	case code >= http.StatusInternalServerError:
		lvl = logrus.ErrorLevel
	case code >= http.StatusBadRequest:
		lvl = logrus.WarnLevel
	default:
		lvl = logrus.InfoLevel
	}

	logrus.StandardLogger().WithFields(logrus.Fields{
		"method": r.Method,
		"uri":    r.RequestURI,
		"status": code,
		"remote": r.RemoteAddr,
	}).Log(lvl, err)
}

func (h handler) find(root, order string, preview bool) ([]Entry, error) {
	entries := make([]Entry, 0)

	// Open folder
	f, err := os.Open(root)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// List folder entries
	fileInfo, err := f.Readdir(-1)
	if err != nil {
		return nil, err
	}

	// Ignored files
	ignored := append([]string{}, h.Ignore...)

	// Append folder entry
	for _, file := range fileInfo {
		// Ignore underscore file prefix
		if strings.HasPrefix(file.Name(), "_") {
			continue
		}

		// Ignore file extension
		if gommon.Has(filepath.Ext(file.Name()), h.SkipExtensions...) {
			continue
		}

		// Ignore ignored file list
		if gommon.Has(file.Name(), ignored...) {
			continue
		}

		// Set entry
		entry := Entry{
			IsDir: file.IsDir(),
			Name:  file.Name(),
			Path:  filepath.Join(root, file.Name()),
			Time:  file.ModTime(),
			Size:  SizeHumanize(file.Size()),
		}

		// Set entry icon
		entry.Icon = gommon.Ternary(entry.IsDir, "fas fa-folder-open", FileIcon(filepath.Ext(entry.Name)))

		// Set entry image
		if preview {
			if entry.IsDir {
				if v := HasFile(entry.Path, RepeatWithSuffix("_thumbnail", imageExt...)...); v != "" {
					entry.Image = filepath.Join(entry.Name, v)
				}
			} else {
				if gommon.Has(filepath.Ext(entry.Name), imageExt...) {
					entry.Image = entry.Name
				} else if v := HasFile(filepath.Dir(entry.Path), RepeatWithSuffix(entry.Name, imageExt...)...); v != "" {
					entry.Image = v
					ignored = append(ignored, entry.Image)
				}
			}
		}

		entries = append(entries, entry)
	}

	// Remove ignored files
	for _, name := range ignored {
		for i, entry := range entries {
			if strings.EqualFold(name, entry.Name) {
				entries = append(entries[:i], entries[i+1:]...)
			}
		}
	}

	// Sort
	switch strings.ToLower(order) {
	case "name":
		sort.Sort(ByName(entries))
	case "date":
		sort.Sort(ByDate(entries))
	}

	return entries, nil
}

func SizeHumanize(b int64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %cB", float64(b)/float64(div), "kMGTPE"[exp])
}

func HasFile(root string, files ...string) string {
	for _, file := range files {
		if _, err := os.Stat(filepath.Join(root, file)); !os.IsNotExist(err) {
			return file
		}
	}

	return ""
}

func RepeatWithSuffix(str string, suffixes ...string) []string {
	list := make([]string, 0)

	for _, suffix := range suffixes {
		list = append(list, str+suffix)
	}

	return list
}

func FileIcon(ext string) string {
	switch {
	case gommon.Has(ext, pdfExt...):
		return "fas fa-file-pdf"
	case gommon.Has(ext, csvExt...):
		return "fas fa-file-csv"
	case gommon.Has(ext, textExt...):
		return "fas fa-file-lines"
	case gommon.Has(ext, imageExt...):
		return "fas fa-file-image"
	case gommon.Has(ext, audioExt...):
		return "fas fa-file-audio"
	case gommon.Has(ext, videoExt...):
		return "fas fa-file-video"
	case gommon.Has(ext, archiveExt...):
		return "fas fa-file-zipper"
	case gommon.Has(ext, securityExt...):
		return "fas fa-file-shield"
	case gommon.Has(ext, codeExt...):
		return "fas fa-file-code"
	case gommon.Has(ext, wordExt...):
		return "fas fa-file-word"
	case gommon.Has(ext, excelExt...):
		return "fas fa-file-excel"
	case gommon.Has(ext, powerpointExt...):
		return "fas fa-file-powerpoint"
	default:
		return "fas fa-file"
	}
}

type ByDate []Entry

func (e ByDate) Len() int           { return len(e) }
func (e ByDate) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e ByDate) Less(i, j int) bool { return e[i].Time.After(e[j].Time) }

type ByName []Entry

func (e ByName) Len() int           { return len(e) }
func (e ByName) Swap(i, j int)      { e[i], e[j] = e[j], e[i] }
func (e ByName) Less(i, j int) bool { return strings.ToLower(e[i].Name) < strings.ToLower(e[j].Name) }
